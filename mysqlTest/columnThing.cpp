#include <cstdint>
#include <mysql/mysql.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>

typedef struct dbConnection{
    const char *userName, *userPassword, *database, *host;
} dbConnection;

MYSQL* mySQLConnectionSetup(dbConnection mariaConnection);
MYSQL_RES* runMySQLQuery(MYSQL* connection, const char *sqlQuery);

int main(void){
    dbConnection testConnect;
    testConnect.database = "pierMarket";
    testConnect.host = "localHost";
    testConnect.userName = "cpp_user";
    testConnect.userPassword = "cpp_maria";
    MYSQL* con = mySQLConnectionSetup(testConnect);
    MYSQL_RES* res = runMySQLQuery(con, "select nome,passwd,id,cpf,permicoes from usuario where id = 1;");
    unsigned int n = mysql_num_fields(res);
    printf("%u\n", n);
    if(n > 0){
        MYSQL_ROW row;
        row = mysql_fetch_row(res);
        for(uint8_t i = 0; i < n; i++) printf("%s\n", row[i]);
    }
    return 0;
}

MYSQL* mySQLConnectionSetup(dbConnection mariaConnection){
    MYSQL* connection =  mysql_init(NULL);
    if(!mysql_real_connect(connection, mariaConnection.host, mariaConnection.userName, mariaConnection.userPassword, mariaConnection.database, 
    0, NULL, 0)){
        printf("Error at connecting with mysql\nmysql error:%s\n", mysql_error(connection));
        exit(1);
    }
    return connection;
}

MYSQL_RES* runMySQLQuery(MYSQL* connection, const char *sqlQuery){
    if(mysql_query(connection, sqlQuery)){
        printf("Error at the query\nmysql erro%s\n", mysql_error(connection));
        exit(1);
    }
    return mysql_use_result(connection);
}
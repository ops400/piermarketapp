#include <mysql/mysql.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>

typedef struct dbConnection{
    const char *userName, *userPassword, *database, *host;
} dbConnection;

MYSQL* mySQLConnectionSetup(dbConnection mariaConnection);
MYSQL_RES* runMySQLQuery(MYSQL* connection, const char *sqlQuery);

int main(void){
    dbConnection testConnect;
    testConnect.database = "pierMarket";
    testConnect.host = "localHost";
    testConnect.userName = "cpp_user";
    testConnect.userPassword = "cpp_maria";
    MYSQL* con = mySQLConnectionSetup(testConnect);
    MYSQL_RES* res = runMySQLQuery(con, "select nome from usuario where id = 2;");
    unsigned long long n = mysql_num_rows(res);
    printf("%llu\n", n);
    // if a empty set is stored in a MYSQL_ROW is going to fuck everything up
    // so use first a unsigned long long to check if it is an empty set
    // the if it's more than 0 then store something to a MYSQL_ROW
    return 0;
}

MYSQL* mySQLConnectionSetup(dbConnection mariaConnection){
    MYSQL* connection =  mysql_init(NULL);
    if(!mysql_real_connect(connection, mariaConnection.host, mariaConnection.userName, mariaConnection.userPassword, mariaConnection.database, 
    0, NULL, 0)){
        printf("Error at connecting with mysql\nmysql error:%s\n", mysql_error(connection));
        exit(1);
    }
    return connection;
}

MYSQL_RES* runMySQLQuery(MYSQL* connection, const char *sqlQuery){
    if(mysql_query(connection, sqlQuery)){
        printf("Error at the query\nmysql erro%s\n", mysql_error(connection));
        exit(1);
    }
    return mysql_use_result(connection);
}
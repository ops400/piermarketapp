#ifndef DBTHINGS_HPP_
#define DBTHINGS_HPP_
#include <cstdint>
#include <mysql/mysql.h>

// borrowed from another project

typedef struct dbConnection{
    const char *userName, *userPassword, *database, *host;
} dbConnection;

typedef struct pesoProd{
    char *nome, *ultDatLeva;
    unsigned int quantEstoque, idCode;
    double precoPorKG;
} pesoProd;

typedef struct produtoGeral{
    unsigned long int eanCode;
    double preco;
    unsigned int quantEstoque;
    char *nome, *ultDatLeva;
} produtoGeral;

typedef struct usuario{
    char nome[100];
    char passwd[32];
    unsigned short int id;
    char cpf[14];
    unsigned char permicoes;
} usuario;

// mySQL/mariaDB things
MYSQL* mySQLConnectionSetup(dbConnection mariaConnection);
MYSQL_RES* runMySQLQuery(MYSQL* connection, const char *sqlQuery);
const char* mySQLSearch(const char* property, const char* id);
// send info to database
void registerUsuario(usuario dummy, dbConnection passTrough);
void addPesoProd(pesoProd dummy, dbConnection passTrough);
void addProdutoGeral(produtoGeral dummy, dbConnection passTrough);
// receive info from database
usuario fetchUserInfo(unsigned short int id, dbConnection passTrough);
pesoProd fetchPesoProd(unsigned int idCode, dbConnection passTrough);
produtoGeral fetchProdutoGeral(unsigned long int eanCode, dbConnection passTrough);

// Just to make things quicker
const dbConnection mainConn{"cpp_user", "cpp_maria", "pierMarket", "localhost"};
#endif

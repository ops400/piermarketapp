#include <raylib.h>
#include "deps/imgui.h"
#include "deps/rlImGui.h"
#include "ui.hpp"

int main(int argc, char** argv){
    unsigned int windowStartFlags = FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT;
    SetConfigFlags(windowStartFlags);
    InitWindow(640, 480, "stock");
    rlImGuiSetup(true);

    uiLoop();

    rlImGuiShutdown();
    CloseWindow();

    return 0;
}

// dbConnection testConnect;
// testConnect.database = "pierMarket";
// testConnect.host = "localHost";
// testConnect.userName = "cpp_user";
// testConnect.userPassword = "cpp_maria";
// MYSQL* connection = mySQLConnectionSetup(testConnect);
// MYSQL_RES* res;
// MYSQL_ROW row;
// res = runMySQLQuery(connection, "select cpf from usuario where nome = 'absolute';");
// row = mysql_fetch_row(res);
// printf("%s\n", row[0]);
// mysql_free_result(res);
// res = runMySQLQuery(connection, "select preco from produtoGeral where eanCode = 7891079000038;");
// row = mysql_fetch_row(res);
// printf("%s\n", row[0]);

#include <cstdio>
#include <cstring>
#include <raylib.h>
#include "deps/imgui.h"
#include "deps/rlImGui.h"
#include "ui.hpp"
#include "dbThings.hpp"
#include "misc.hpp" // temp (or not idk)

void uiLoop(void){
    // char test[6];
    // strcpy(test, "31.42");
    ImGuiIO& io = ImGui::GetIO();
    (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.MouseDrawCursor = true;
    
    ImGuiWindowFlags imGuiWindowFlagsForStockWindow = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_NoBringToFrontOnFocus;
    bool openStockWindow = true;
    // usuario testUser = fetchUserInfo(1, mainConn);
    pesoProd testPesoProd = fetchPesoProd(1, mainConn);
    // test purposes
    // double test = charArrayDouble((char*)"31.42");
    // printf("DEBUG_charArrayDouble_TEST: %f\n", test);
    // unsigned long int tInt = charArrayInt((char*)"1");
    // printf("DEBUG_charArrayInt_TEST: %lu\n", tInt);
    while(!WindowShouldClose()){
        BeginDrawing();
        ClearBackground(BLACK);
        rlImGuiBegin();
        
        ImGui::SetNextWindowBgAlpha(1.0f);
        ImGui::SetNextWindowSize(io.DisplaySize);
        ImGui::SetNextWindowPos({0, 0});
        if(ImGui::Begin("stock", &openStockWindow, imGuiWindowFlagsForStockWindow)){
            ImGui::TextUnformatted("Hello");
            // ImGui::Text("%s", testUser.nome);
            ImGui::Text("%s", testPesoProd.nome);
            ImGui::Text("%.2f", testPesoProd.precoPorKG);
            ImGui::End();
        }

        // DrawFPS(0, 0);
        rlImGuiEnd();
        EndDrawing();
    }

    return;
}

// 7895800430002
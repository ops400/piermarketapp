#include "misc.hpp"
#include <cstdio>
#include <cstring>

unsigned long int simplePow(unsigned int base, unsigned int exponent){
    unsigned long int final = 0;
    switch(exponent){
        case 0:
            return 1;
            break;
        case 1:
            return base;
            break;
        default:
            final = base;
            for(unsigned int i = 2; i < exponent; i++){
                final *= base;
            }
            return final;
    }
    return 0;
}

unsigned long int charArrayInt(char* s){
    unsigned int sLength = strlen(s);
    char sCopy[sLength];
    charCleaner(sCopy);
    strcpy(sCopy, s);
    unsigned short int final = 0;
    invertCharArray(sCopy);
    for(unsigned int i = 0; i < sLength; i++){
        final += (sCopy[i]-48)*simplePow(10, i);
    }
    return final;
}

double charArrayDouble(char* s){
    printf("DEBUG_charArrayDouble_1\n");
    double final = 0.0f;
    unsigned int sLength = strlen(s);
    char sCopy[sLength];
    charCleaner(sCopy);
    strcpy(sCopy, s);
    invertCharArray(sCopy);
    for(unsigned int i = 0; i < sLength; i++){
        switch(i){
            case 0:
                final += (double)(sCopy[i]-48)/100;
                break;
            case 1:
                final += (double)(sCopy[i]-48)/10;
                break;
            case 2:
                ;
                break;
            default:
                final += (double)(sCopy[i]-48)*simplePow(10, i-3);
        }
    }
    return final;
    // return 0.0f;
}

void invertCharArray(char* s){
    unsigned int sLength = strlen(s);
    unsigned int end = sLength-1;
    unsigned int start = 0;
    char tempHolder = 0;
    while(start < end){
        tempHolder = s[end];
        s[end] = s[start];
        s[start] = tempHolder;
        end--;
        start++;
    }
    return;
}

void charCleaner(char* s){
    for(unsigned int i = 0; i < strlen(s); i++){
        s[i] = 0;
    }
    return;
}


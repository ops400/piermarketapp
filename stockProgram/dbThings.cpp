#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <mysql/mysql.h>
#include <string>
#include <sys/types.h>
#include "dbThings.hpp"
#include "misc.hpp"

MYSQL* mySQLConnectionSetup(dbConnection mariaConnection){
    MYSQL* connection =  mysql_init(NULL);
    if(!mysql_real_connect(connection, mariaConnection.host, mariaConnection.userName, mariaConnection.userPassword, mariaConnection.database, 
    0, NULL, 0)){
        printf("Error at connecting with mysql\nmysql error:%s\n", mysql_error(connection));
        exit(1);
    }
    return connection;
}

MYSQL_RES* runMySQLQuery(MYSQL* connection, const char *sqlQuery){
    if(mysql_query(connection, sqlQuery)){
        printf("Error at the query\nmysql erro%s\n", mysql_error(connection));
        exit(1);
    }
    return mysql_use_result(connection);
}

void registerUsuario(usuario dummy, dbConnection passTrough){
    char sendQuery[400];
    for(uint i = 0; i < sizeof(sendQuery); i++) sendQuery[i] = 0;

    sprintf(sendQuery, "insert into usuario (nome, passwd, cpf, permicoes) values('%s', '%s', '%s', %u)", dummy.nome, dummy.passwd, dummy.cpf, dummy.permicoes);
    MYSQL* connection = mySQLConnectionSetup(passTrough);
    MYSQL_RES* result = runMySQLQuery(connection, sendQuery);
    mysql_free_result(result);

    return;
}

void addPesoProd(pesoProd dummy, dbConnection passTrough){
    char sendQuery[500];
    for(uint i = 0; i < sizeof(sendQuery); i++) sendQuery[i] = 0;

    sprintf(sendQuery, "insert into pesoProd (idCode, nome, quantEstoque, ultDatLeva, precoPorKG) values(%u, '%s', %u, '%s', %.2f)", 
    dummy.idCode, dummy.nome, dummy.quantEstoque, dummy.ultDatLeva, dummy.precoPorKG);
    MYSQL* connection = mySQLConnectionSetup(passTrough);
    MYSQL_RES* result = runMySQLQuery(connection, sendQuery);
    mysql_free_result(result);

    return;
}

void addProdutoGeral(produtoGeral dummy, dbConnection passTrough){
    char sendQuery[600];
    for(uint i = 0; i < sizeof(sendQuery); i++) sendQuery[i] = 0;

    sprintf(sendQuery, "insert into produtoGeral (eanCode, preco, quantEstoque, ultDatLeva, nome) values (%lu, %.2f, %u, '%s', '%s')",
    dummy.eanCode, dummy.preco, dummy.quantEstoque, dummy.ultDatLeva, dummy.nome);
    MYSQL* connection = mySQLConnectionSetup(passTrough);
    MYSQL_RES* result =  runMySQLQuery(connection, sendQuery);
    mysql_free_result(result);

    return;
}

// insert into produtoGeral (eanCode, preco, quantEstoque, ultDatLeva, nome) values (7891079000038, 3.55, 5, '2023-12-16', 'Nissin Miojo galinha caipira');
// insert into pesoProd (precoPorKG, ultDatLeva, nome, quantEstoque, idCode) value (11.12, '2023-12-16', 'Laranja', 50, 1);
// insert into usuario (nome, passwd, cpf) values('absolute', 'abs0', '000.000.000-00');

usuario fetchUserInfo(unsigned short int id, dbConnection passTrough){
    printf("DEBUG_fetchUserInfo_id: %u\n", id);
    usuario passTroughUser;
    MYSQL* connection = mySQLConnectionSetup(passTrough);
    char passTroughSQLQuery[1000];
    charCleaner(passTroughSQLQuery);
    sprintf(passTroughSQLQuery, "select nome,passwd,id,cpf,permicoes from usuario where id = %u", id);
    MYSQL_RES* result = runMySQLQuery(connection, passTroughSQLQuery);
    // unsigned long long int nOfFields = mysql_num_fields(result);
    // printf("DEBUG_fetchUserInfo_nOfFields: %llu\n", nOfFields);
    MYSQL_ROW row = mysql_fetch_row(result);
    // if(row == NULL) printf("fuckkkkkkk\n");
    // the above method is the correct one to check if it is an empty set
    if(row != NULL){
        // MYSQL_ROW row = mysql_fetch_row(result);
        strcpy(passTroughUser.nome, row[0]);
        strcpy(passTroughUser.passwd, row[1]);
        printf("DEBUG_fetchUserInfo_1\n");
        passTroughUser.id = charArrayInt(row[2]);
        
        // test purposes
        // unsigned int sLength = strlen(row[2]);
        // for(unsigned int i = 0; i < sLength; i++){
            // printf("%c:%u", row[2][i], row[2][i]);
        // }
        // printf("\n");
        
        printf("DEBUG_fetchUserInfo_2\n");
        printf("%s\n%u\n", row[2], passTroughUser.id);
        strcpy(passTroughUser.cpf, row[3]);
        passTroughUser.permicoes = charArrayInt(row[4]);
    }
    else{
        strcpy(passTroughUser.nome, "a");
        strcpy(passTroughUser.passwd, "a");
        passTroughUser.id = 0;
        strcpy(passTroughUser.cpf, "a");
        passTroughUser.permicoes = 99;
    }
    printf("HEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE\n");
    mysql_free_result(result);
    return passTroughUser;
}

pesoProd fetchPesoProd(unsigned int idCode, dbConnection passTrough){
    printf("DEBUG_fetchPesoProd_idCode: %u\n", idCode);
    pesoProd passTroughPesoProd;
    MYSQL* connection = mySQLConnectionSetup(passTrough);
    printf("DEBUG_fetchPesoProd_1\n");
    char passTroughSQLQuery[1000];
    charCleaner(passTroughSQLQuery);
    sprintf(passTroughSQLQuery, "select precoPorKG,ultDatLeva,nome,quantEstoque,idCode from pesoProd where idCode = %u", idCode);
    MYSQL_RES* result = runMySQLQuery(connection, passTroughSQLQuery);
    printf("DEBUG_fetchPesoProd_2\n");
    MYSQL_ROW row = mysql_fetch_row(result);
    printf("DEBUG_fetchPesoProd_3\n");
    if(row != NULL){
        passTroughPesoProd.precoPorKG = charArrayDouble(row[0]);
        printf("DEBUG_fetchPesoProd_4\n");
        strcpy(passTroughPesoProd.ultDatLeva, row[1]);
        printf("DEBUG_fetchPesoProd_5\n");
        printf("row[2]:%s\n", row[2]);
        strcpy(passTroughPesoProd.nome, row[2]);
        printf("DEBUG_fetchPesoProd_6\n");
        passTroughPesoProd.quantEstoque = charArrayInt(row[3]);
        printf("DEBUG_fetchPesoProd_7\n");
        passTroughPesoProd.idCode = charArrayInt(row[4]);
        printf("DEBUG_fetchPesoProd_8\n");
    }
    else{
        passTroughPesoProd.idCode = 0;
        strcpy(passTroughPesoProd.nome, "a");
        strcpy(passTroughPesoProd.ultDatLeva, "0-0-0");
        passTroughPesoProd.precoPorKG = 0;
        passTroughPesoProd.quantEstoque = 0;
    }
    mysql_free_result(result);
    return passTroughPesoProd;
}

#ifndef MISC_HPP_
#define MISC_HPP_

unsigned long int charArrayInt(char* s);
unsigned long int simplePow(unsigned int base, unsigned int exponent);
double charArrayDouble(char* s);
void invertCharArray(char* s);
void charCleaner(char* s);

#endif